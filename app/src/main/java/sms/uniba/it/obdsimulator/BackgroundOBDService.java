package sms.uniba.it.obdsimulator;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.Charset;
import java.text.DecimalFormat;
import java.util.Random;

public class BackgroundOBDService extends Service {

    public Context context = this;
    public Handler handler = null;
    public Runnable runnable = null;

    private OBD obd = new OBD();
    DecimalFormat df = new DecimalFormat("#.######");
    private int i = 0;
    JSONObject jsonObject = new JSONObject();
    StringBuilder stringBuild = new StringBuilder();
    byte[] bytes = new byte[1024];


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        Toast.makeText(this, "Servizio Realizzato", Toast.LENGTH_SHORT).show();
        obd.populateOBDStats();

        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                //Toast.makeText(context, "Invio sessione  " + i, Toast.LENGTH_SHORT).show();

                Random random = new Random();
                OBDStats stat = obd.getList().get(i);



                try {
                    jsonObject.put("GPSLongitude", df.format(stat.getGPSLongitude()));
                    jsonObject.put("GPSLatitude", df.format(stat.getGPSLatitude()));

                    jsonObject.put("Speed", df.format(stat.getOBDSpeed()));

                    jsonObject.put("EngineRPM", stat.toStringEngineRPM());
                    jsonObject.put("EngineLoad", df.format(stat.getEngineLoad()));
                    jsonObject.put("FuelFlowRate", stat.toStringFuelFlowRate());
                    //	jsonObject.put("ThrottlePosition", stat.toStringThrottlePosition());
                    //	jsonObject.put("RelativeThrottlePosition", stat.toStringRelativeThrottlePosition());
                    jsonObject.put("AcceleratorPedalRelativePosition", stat.toStringAcceleratorPedalRelativePosition());
                    jsonObject.put("PendenzaStrada", stat.toStringPendenzaStrada());
                    jsonObject.put("Rettilineo", stat.toStringRettilineo());
                    jsonObject.put("TipoStrada", stat.getTipoStrada());
                    Log.d("SALAME", jsonObject.toString());
                    MainActivity.mBluetoothConnection.write(jsonObject.toString().getBytes(Charset.defaultCharset()));
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                handler.postDelayed(runnable, 2000);
                i++;
            }
        };

        handler.postDelayed(runnable, 5000);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        handler.removeCallbacks(runnable);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(this, "Servizio avviato", Toast.LENGTH_SHORT).show();
        return START_NOT_STICKY;
    }
}
