package sms.uniba.it.obdsimulator;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by feder on 20/06/2018.
 */

public class OBD {

    private ArrayList<OBDStats> obdstats;

    OBD() {
        obdstats = new ArrayList<>();
    }

    public ArrayList<OBDStats> getList() {
        return obdstats;
    }

    public void populateOBDStats() {
        Random random = new Random();

        double startLatitude = random.nextDouble() + random.nextInt(90);
        double startLongitude = random.nextDouble() + random.nextInt(90);

        double endLatitude = startLatitude + (1 + random.nextDouble());
        double endLongitude = startLongitude + (1 + random.nextDouble());

        double prevLatitude = startLatitude;
        double prevLongitude = startLongitude;

        double prevSpeed = 0;
        int prevRPM = 1000;
        double prevFuelRate = 1 + random.nextDouble();

        int speedIncrement = 5;
        int rpmIncrement = 10;
        int fuelIncrement = 1;

        int i = 0;
        int k = 0;
        boolean sceltaStrada = false;
        String tipoStrada = "";
        int probRettilineo = 0;
        int precPendenza = 0;


        while (i < 150) {
            OBDStats obdstat = new OBDStats();

            obdstat.randomSetLatitude(prevLatitude, endLatitude);
            obdstat.randomSetLongitude(prevLongitude, endLongitude);

            //se non è stata ancora scelta una strada
            if (k == 0) {
                sceltaStrada = false;
                precPendenza = 0;
            }

            if (!sceltaStrada) {
                k = random.nextInt(4);
                switch (k) {
                    //urbana
                    case 0:
                        k = 3;  //numero di rilevazioni in urbana
                        tipoStrada = "StradaUrbana";
                        break;
                    //extraurbana SECONDARIA
                    case 1:
                        k = 20;
                        tipoStrada = "StradaExtraurbanaSec";
                        break;
                    //extraurbana PRINCIPALE
                    case 2:
                        k = 30;
                        tipoStrada = "StradaExtraurbanaPrinc";
                        break;
                    //autostrada
                    case 3:
                        k = 50;
                        tipoStrada = "Autostrada";
                        break;
                }
                sceltaStrada = true;
            }

            switch (tipoStrada) {

                case "StradaUrbana":
                    obdstat.setTipoStrada("StradaUrbana");
                    if (precPendenza < 0) {
                        obdstat.setPendenzaStrada(precPendenza + 1);
                    } else if (precPendenza == 0) {
                        obdstat.setPendenzaStrada(random.nextInt(21) - 10);
                    } else {
                        obdstat.setPendenzaStrada(precPendenza - 1);
                    }
                    obdstat.setRettilineo(random.nextBoolean());
                    break;
                case "StradaExtraurbanaSec":
                    probRettilineo = random.nextInt(10);
                    if (probRettilineo <= 5) {
                        obdstat.setRettilineo(true);
                    } else {
                        obdstat.setRettilineo(false);
                    }
                    obdstat.setTipoStrada("StradaExtraurbanaSec");
                    if (precPendenza < 0) {
                        obdstat.setPendenzaStrada(precPendenza + 1);
                    } else if (precPendenza == 0) {
                        obdstat.setPendenzaStrada(random.nextInt(13) - 6);
                    } else {
                        obdstat.setPendenzaStrada(precPendenza - 1);
                    }
                    break;
                case "StradaExtraurbanaPrinc":
                    probRettilineo = random.nextInt(10);
                    if (probRettilineo <= 6) {
                        obdstat.setRettilineo(true);
                    } else {
                        obdstat.setRettilineo(false);
                    }
                    obdstat.setTipoStrada("StradaExtraurbanaPrinc");
                    if (precPendenza < 0) {
                        obdstat.setPendenzaStrada(precPendenza + 1);
                    } else if (precPendenza == 0) {
                        obdstat.setPendenzaStrada(random.nextInt(13) - 6);
                    } else {
                        obdstat.setPendenzaStrada(precPendenza - 1);
                    }
                    break;
                case "Autostrada":
                    probRettilineo = random.nextInt(10);
                    if (probRettilineo <= 7) {
                        obdstat.setRettilineo(true);
                    } else {
                        obdstat.setRettilineo(false);
                    }
                    obdstat.setTipoStrada("Autostrada");
                    if (precPendenza < 0) {
                        obdstat.setPendenzaStrada(precPendenza + 1);
                    } else if (precPendenza == 0) {
                        obdstat.setPendenzaStrada(random.nextInt(13) - 6);
                    } else {
                        obdstat.setPendenzaStrada(precPendenza - 1);
                    }

                    break;
            }

            // se sto fermo
            if (obdstat.getGPSLatitude() == prevLatitude && obdstat.getGPSLongitude() == prevLongitude) {
                obdstat.setOBDSpeed(0);
                obdstat.setEngineRPM(0);
                obdstat.setFuelFlowRate(0);
                obdstat.setEngineLoad(0);
            } else {
                int dice = random.nextInt(10);
                //1/9 rallenta
                if (dice >= 8) {
                    speedIncrement = random.nextInt(20) * -1;
                    rpmIncrement = random.nextInt(14) * -1;
                    obdstat.randomGPSSpeed(speedIncrement, prevSpeed);
                    obdstat.randomEngineRPM(rpmIncrement, prevRPM);
                    //	fuelIncrement -= fuelIncrement * ((obdstat.getOBDSpeed() - prevSpeed) + (obdstat.getEngineRPM()-prevRPM)*100/16000);
                    //	obdstat.randomFuelRate(fuelIncrement, prevFuelRate);
                    obdstat.calculateFuelRate();
                    obdstat.calculateEngineLoad();
                }
                // 6/9 accelera
                else if (dice <= 6) {
                    speedIncrement = random.nextInt(9);
                    rpmIncrement = random.nextInt(8) + 1;

                    obdstat.randomGPSSpeed(speedIncrement, prevSpeed);
                    obdstat.randomEngineRPM(rpmIncrement, prevRPM);
                    //	fuelIncrement += fuelIncrement * ((obdstat.getOBDSpeed() - prevSpeed) + (obdstat.getEngineRPM()-prevRPM)*100/16000);
                    //	fuelIncrement = (int) (((obdstat.getOBDSpeed()*3276*4)/255)+((obdstat.getEngineRPM()*3276*6)/16383)/10);
                    //	obdstat.randomFuelRate(fuelIncrement, prevFuelRate);
                    obdstat.calculateFuelRate();
                    obdstat.calculateEngineLoad();
                } else {
                    obdstat.randomGPSSpeed(0, prevSpeed);
                    obdstat.randomEngineRPM(0, prevRPM);
                    obdstat.randomFuelRate(0, prevFuelRate);
                    obdstat.calculateEngineLoad();
                }

            }

            prevSpeed = obdstat.getOBDSpeed();
            prevRPM = obdstat.getEngineRPM();
            prevFuelRate = obdstat.getFuelFlowRate();
            prevLatitude = obdstat.getGPSLatitude();
            prevLongitude = obdstat.getGPSLongitude();
            precPendenza = obdstat.getPendenzaStrada();

            obdstats.add(obdstat);
            i++;
            k--;

        }

    }


}
