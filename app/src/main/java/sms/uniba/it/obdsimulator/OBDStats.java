package sms.uniba.it.obdsimulator;

import android.util.Log;

import java.util.Random;

/**
 * Created by feder on 20/06/2018.
 */

public class OBDStats {
    private double GPSLongitude;
    private double GPSLatitude;
    private double OBDSpeed;
    private int engineRPM;
    private double engineLoad;
    private double fuelFlowRate;
    private double acceleratorPedalRelativePosition;
    private int pendenzaStrada;
    private boolean rettilineo;
    private String tipoStrada;



    // valori possibili di tipo strada
    // Autostrada, StradaExtraurbanaPrinc, StradaExtraurbanaSec, StradaUrbana


    public OBDStats() {


    }

    public void calculateEngineLoad() {
        engineLoad = ((OBDSpeed * 100 / 255)*2)+((engineRPM*100/16383)*5)+((acceleratorPedalRelativePosition)*3)/10;
        Log.d("ENGINELOAD: ", Double.toString(engineLoad));
    }

    public void randomSetLongitude(double min,double max) {
        Random random = new Random();
        do {
            GPSLongitude =  min;
            GPSLongitude +=random.nextGaussian()/505;
        } while (GPSLongitude < min && GPSLongitude > max);
        Log.d("LONGITUDINE: ", Double.toString(GPSLongitude));
    }

    public void randomSetLatitude(double min,double max) {
        Random random = new Random();
        do {
            GPSLatitude =  min;
            GPSLatitude +=random.nextGaussian()/505;
        } while (GPSLatitude < min && GPSLatitude > max);
        Log.d("LATITUDINE: ", Double.toString(GPSLatitude));

    }


    public void randomGPSSpeed(int increment, double prevSpeed) {
        Random random = new Random();
        if (increment == 0) {
            OBDSpeed = prevSpeed;
        }
        else if (increment < 0) {
            OBDSpeed = prevSpeed + increment;
        }
        else if (increment > 0 && prevSpeed >=130){
            OBDSpeed = random.nextInt(5) + prevSpeed;
        }

        else if (increment > 0 && prevSpeed < 130){
            OBDSpeed = increment + prevSpeed;
        }


        if (OBDSpeed >= 255)
            OBDSpeed = 255;
        if (OBDSpeed <= 0)
            OBDSpeed = 0;
        calculateAcceleratorPedal(prevSpeed);
        Log.d("ODBSPEED: ", Double.toString(OBDSpeed));

    }

    public void randomEngineRPM(int increment, int prevRPM) {
        Random random = new Random();
        if (increment == 0)
            engineRPM = prevRPM;
        else
            engineRPM = prevRPM + (increment * 10);
        if (engineRPM >= 16383 )
            engineRPM = 16383;
        if (engineRPM < 0)
            engineRPM = 0;
        Log.d("ENGINERPM: ", Double.toString(engineRPM));
    }



    public void calculateFuelRate() {
        fuelFlowRate=(this.getOBDSpeed()*30*4/255)+((this.getEngineRPM()*20*6)/16383)/10;
    }

    public void randomFuelRate(int increment, double prevFuelRate) {
        if (increment == 0)
            fuelFlowRate = prevFuelRate - (prevFuelRate*0.08);
        else
            fuelFlowRate = prevFuelRate + (increment * 10);


        if (fuelFlowRate > 3276)
            fuelFlowRate = 3276;
        if (fuelFlowRate < 0)
            fuelFlowRate = 0;
        Log.d("FUELFLOWRATE: ", Double.toString(fuelFlowRate));

    }



    private void calculateAcceleratorPedal(double prevSpeed) {
        double temp = this.getOBDSpeed() - prevSpeed;
        if (getOBDSpeed()==0)
            acceleratorPedalRelativePosition = 0;
        else if (prevSpeed <= 40)
            acceleratorPedalRelativePosition = ((Math.abs(prevSpeed-getOBDSpeed()))*100/(prevSpeed+28));
        else if (prevSpeed >41)
            acceleratorPedalRelativePosition = 32+((Math.abs(prevSpeed-getOBDSpeed()))*100/prevSpeed);
        Log.d("ACCELERATORPEDALRELATIVEPOSITION: ", Double.toString(acceleratorPedalRelativePosition));

    }

    public double getGPSLongitude() {
        return GPSLongitude;
    }

    public String toStringGPSLongitude() {
        return String.valueOf(GPSLongitude);
    }

    public void setGPSLongitude(double gPSLongitude) {
        GPSLongitude = gPSLongitude;
    }

    public double getGPSLatitude() {
        return GPSLatitude;
    }

    public String toStringGPSLatitude() {
        return String.valueOf(GPSLatitude);
    }

    public void setGPSLatitude(double gPSLatitude) {
        GPSLatitude = gPSLatitude;
    }

    public double getOBDSpeed() {
        return OBDSpeed;
    }

    public String toStringOBDSpeed() {
        return String.valueOf(OBDSpeed);
    }

    public void setOBDSpeed(double obdSpeed) {
        OBDSpeed = obdSpeed;
    }

    public double getEngineLoad() {
        return engineLoad;
    }

    public String toStringEngineLoad() {
        return String.valueOf(engineLoad);
    }

    public void setEngineLoad(double engineLoad) {
        this.engineLoad = engineLoad;
    }

    public double getFuelFlowRate() {
        return fuelFlowRate;
    }

    public String toStringFuelFlowRate() {
        return String.valueOf(fuelFlowRate);
    }

    public void setFuelFlowRate(double fuelFlowRate) {
        this.fuelFlowRate = fuelFlowRate;
    }

    public double getAcceleratorPedalRelativePosition() {
        return acceleratorPedalRelativePosition;
    }

    public String toStringAcceleratorPedalRelativePosition() {
        return String.valueOf(acceleratorPedalRelativePosition);
    }

    public void setAcceleratorPedalRelativePosition(double acceleratorPedalRelativePosition) {
        this.acceleratorPedalRelativePosition = acceleratorPedalRelativePosition;
    }

    public int getEngineRPM() {
        return engineRPM;
    }

    public String toStringEngineRPM() {
        return String.valueOf(engineRPM);
    }

    public void setEngineRPM(int engineRPM) {
        this.engineRPM = engineRPM;
    }

    public int getPendenzaStrada() {
        return pendenzaStrada;
    }

    public String toStringPendenzaStrada(){
        return String.valueOf(pendenzaStrada);
    }

    public void setPendenzaStrada(int pendenzaStrada) {
        this.pendenzaStrada = pendenzaStrada;
    }

    public boolean isRettilineo() {
        return rettilineo;
    }

    public void setRettilineo(boolean rettilineo) {
        this.rettilineo = rettilineo;
    }

    public String toStringRettilineo(){
        if (isRettilineo()){
            return "true";
        } else {
            return "false";
        }
    }

    public String getTipoStrada() {
        return tipoStrada;
    }

    public void setTipoStrada(String tipoStrada) {
        this.tipoStrada = tipoStrada;
    }



}
